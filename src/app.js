import React from 'react';
const remote = require('electron').remote
const main = remote.require('./main.js')
const _ = require('lodash');
const moment = require('moment');

// Clear previous databases
// indexedDB.webkitGetDatabaseNames().onsuccess = function(event) {
//   Array.prototype.forEach.call(event.target.result, indexedDB.deleteDatabase.bind(indexedDB));
// }

// const idb = require('idb')
// var dbPromise = idb.open('db', 1, function(upgradeDb) {
//     if (!upgradeDb.objectStoreNames.contains('entries')) {
//         var keyPath = ['title', 'client', 'started', 'ended']
//         var entriesOS = upgradeDb.createObjectStore('entries', keyPath)
//     }
// });




export default class App extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
          timerActive: this.getTimerStatus(),
          timers: [],
          currentTimer: false
        };
    }
    getTimerStatus() {
        // dbPromise.then(function(db) {
        //   var tx = db.transaction('entries', 'readonly');
        //   var store = tx.objectStore('entries');
        //   return store.get('sandwich');
        // }).then(function(val) {
        //   console.dir(val);
        // });

        return false;
    }
    toggleTimer() {

        if (this.state.timerActive) {
            console.log('stopping timer')

            var current = this.state.currentTimer;
            current.endTime = moment().format();

            var timers = this.state.timers;
            timers.unshift(current);

            this.setState({
                timers: timers,
                currentTimer: false,
                timerActive: !this.state.timerActive
            })
 
        }
        else{
            console.log('starting timer')

            this.setState({
                currentTimer: {
                    startTime: moment().format(),
                    elapsed: 0
                },
                timerActive: !this.state.timerActive
            })
            // dbPromise.then(function(db) {
            //   var tx = db.transaction('entries', 'readwrite');
            //   var store = tx.objectStore('entries');
            //   var item = {
            //     title: 'Coding',
            //     client: "Client A",
            //     started: new Date().getTime()
            //   };
            //   store.add(item);
            //   return tx.complete;
            // }).then(function() {
            //   console.log('added item to the entries os!');
            // });
        }

    }
    tick() {
        if (this.state.timerActive) {
            var currentTimer = this.state.currentTimer;
            currentTimer.elapsed = moment().diff(moment(this.state.currentTimer.startTime), "s");
            this.setState({
                currentTimer: currentTimer
            });
        }
    }
    componentDidMount() {
        this.interval = setInterval(this.tick.bind(this), 1000);
    }
    componentWillUnmount() {
        clearInterval(this.interval);
    }
    render() {
        let currentTimerRender;
        if (this.state.timerActive) {
            currentTimerRender = <div><strong>Current Time: {moment(this.state.currentTimer.startTime).format("MMM DD, YYYY h:mm:ss a")}</strong><p>Elapsed Time: {this.state.currentTimer.elapsed}</p></div>
        }
        let classes = 'btn';
        classes += this.state.timerActive ? ' red' : ' pulse';

        return (
        <div>
            <div className="row">
                <div className="col s12">
                    <h3>Timer is {this.state.timerActive ? "Active" : "Inactive"}</h3>
                </div>
            </div>
            <div className="row">
                <div className="col s12">
                    <button className={classes} onClick={this.toggleTimer.bind(this)}>
                      {this.state.timerActive ? "Stop" : "Start"} Timer
                    </button>
                </div>
            </div>

            {currentTimerRender}
            <ul>
                {this.state.timers.map(function(timer, i) {
                   return (<li key={i}>{moment(timer.startTime).format("MMM DD, YYYY h:mm:ss a")} - {moment(timer.endTime).format("MMM DD, YYYY h:mm:ss a")}</li>);
                },this)}
            </ul>
        </div>
        );
    }
}